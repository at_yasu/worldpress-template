=== world press template ===

Contributors: a.yasui@gmail.com
Tags: one-column, flexible-header, accessibility-ready, custom-colors, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready
Requires at least: 4.9.6
Tested up to: WordPress 5.0
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Our 2019 default theme is designed to show off the power of the block editor.

== Description ==

wppusher を使いたいがために作成したサンプルテンプレート

== Changelog ==

= 1.1 =

* Released: December 19, 2018

https://codex.wordpress.org/Twenty_Nineteen_Theme_Changelog#Version_1.1

= 1.0 =

* Released: December 6, 2018

Initial release

== Resources ==

* normalize.css, © 2012-2018 Nicolas Gallagher and Jonathan Neal, MIT
* Underscores, © 2012-2018 Automattic, Inc., GNU GPL v2 or later
